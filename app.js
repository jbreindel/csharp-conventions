var request = require('request');
var pandoc = require('node-pandoc');
var fs = require('fs');

// for other formats see http://pandoc.org/README.html#options
var FORMAT = 'markdown';
var FORMAT_EXT = '.md';

function convertPage(pageTitle, page, args) {

  console.log('Converting: ' + page);

  // convert to format
  pandoc(page, args, function(err, result) {

    if (err) {
      console.error('Failed to convert: ' + pageTitle);
    }

    console.log('Converted: ' + pageTitle + ' to ' + FORMAT);
    return result;
  });
}

function downloadPage(dir, page, pageLink) {

  var pageLink = page.link;
  var pageTitle = page.title;
  var page = dir + '/' + pageTitle;

  console.log('Downloading page: ' + pageTitle);

  request.get(pageLink, function(error, response, body) {

    var htmlPage = page + '.html'

    // save html page
    fs.writeFileSync(htmlPage, body);

    console.log('Saved html page: ' + htmlPage);

    // convert page
    convertPage(pageTitle, htmlPage, ['-f',
                                      'html',
                                      '-t',
                                      FORMAT,
                                      '-o',
                                      page + FORMAT_EXT]);
  });
}

function downloadSection(dir, section) {

  var sectionDir = dir + '/' + section.name;

  if (fs.existsSync(sectionDir) || fs.mkdirSync(sectionDir)) {

    var pages = section.pages;
    for (var i = 0; i < pages.length; i++) {
      downloadPage(sectionDir, pages[i]);
    }
  }
}

(function() {

  var dir = 'docs';
  var sections = require('./pages.json').sections;

  if (fs.existsSync(dir) || fs.mkdirSync(dir)) {

    for (var i = 0; i < sections.length; i++) {
      var section = sections[i];
      downloadSection(dir, section);
    }
  }
})();
